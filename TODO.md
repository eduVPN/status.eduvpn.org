# TODO

- parallel `curl` so it doesn't take forever
- output various pages, e.g. stuff that we can make public, stuff we need to
  keep private
  - simple online/offline/degraded indicator
  - have a page where people can find their support contact based on same
    search as in the apps (JS?)
