<?php

define('VPN_LATEST_VERSION', ['3.9.11']);

// *** Source Formatting
// $ php-cs-fixer fix --allow-risky --rules @PER-CS,@PER-CS:risky version_info.php
//
// *** Cron
// $ crontab -l
// @hourly	/usr/bin/php /home/fkooman/version_info.php > /var/www/html/fkooman/version_info.html.tmp && mv /var/www/html/fkooman/version_info.html.tmp /var/www/html/fkooman/version_info.html

require_once __DIR__ . '/Minisign.php';

$dateTime = new DateTime();
$configData = include __DIR__ . '/config.php';

$showHistory = false;
$showTechContact = false;
$serverList = [];
$errorList = [];
$serverHistory = [];
$serverHistoryFile = __DIR__ . '/server_history.dat';
if(file_exists($serverHistoryFile)) {
    $serverHistory = unserialize(file_get_contents($serverHistoryFile));
}

if(null !== $configData['discoUrl']) {
    $serverListJson = getUrl($configData['discoUrl']);
    $serverListJsonSig = getUrl($configData['discoUrl'] . '.minisig');
    try {
        $publicKeyList = [];
        if (array_key_exists('publicKeyList', $configData)) {
            $publicKeyList = $configData['publicKeyList'];
        }
        if (0 !== count($publicKeyList)) {
            Minisign::verify($serverListJson, $serverListJsonSig, $configData['publicKeyList']);
        }
        $serverList = json_decode($serverListJson, true)['server_list'];
    } catch (Exception $e) {
        // signature check failed
        // we just won't have any servers...
        $errorList['https://disco.eduvpn.org/'] = 'discovery signature verification failed';
    }
}

// other servers not part of any discovery file
foreach ($configData['otherServerList'] as $otherBaseUrl) {
    $serverList[] = [
        'base_url' => $otherBaseUrl,
        'server_type' => 'alien',
        'display_name' => parse_url($otherBaseUrl, PHP_URL_HOST),
        'support_contact' => [],
    ];
}

// add tech contact
if(array_key_exists('techContactList', $configData)) {
    $showTechContact = true;
    foreach($serverList as $k => $v) {
        $serverList[$k]['tech_contact'] = null;
        if(array_key_exists($v['base_url'], $configData['techContactList'])) {
            $serverList[$k]['tech_contact'] = $configData['techContactList'][$v['base_url']];
        }
    }
}

usort($serverList, function ($a, $b) {
    if ($a['server_type'] === $b['server_type']) {
        // same server type, sort by TLD
        $tldA = getTld($a['base_url']);
        $tldB = getTld($b['base_url']);
        if ($tldA === $tldB) {
            // if both TLDs are the same, sort by display name
            return strcmp(getDisplayName($a), getDisplayName($b));
        }

        return strcmp($tldA, $tldB);
    }

    // make sure "Secure Internet" is on top
    if ('secure_internet' === $a['server_type']) {
        return -1;
    }
    if ('secure_internet' === $b['server_type']) {
        return 1;
    }

    // make sure "Alien" is at the bottom
    if ('alien' === $a['server_type']) {
        return 1;
    }
    if ('alien' === $b['server_type']) {
        return -1;
    }

    return strcmp($a['server_type'], $b['server_type']);
});

function getEmojiFlag(string $countryCode): string
{
    $emojiMapping = [
        'AL' => '🇦🇱',
        'CY' => '🇨🇾',
        'DK' => '🇩🇰',
        'EE' => '🇪🇪',
        'FI' => '🇫🇮',
        'FR' => '🇫🇷',
        'DE' => '🇩🇪',
        'IT' => '🇮🇹',
        'MY' => '🇲🇾',
        'MA' => '🇲🇦',
        'NO' => '🇳🇴',
        'KE' => '🇰🇪',
        'PK' => '🇵🇰',
        'ZA' => '🇿🇦',
        'LK' => '🇱🇰',
        'NL' => '🇳🇱',
        'UG' => '🇺🇬',
        'UA' => '🇺🇦',
        'ET' => '🇪🇹',
        'LU' => '🇱🇺',
    ];

    if(array_key_exists($countryCode, $emojiMapping)) {
        return $emojiMapping[$countryCode] . ' (' . $countryCode . ')';
    }

    return $countryCode;
}

function getTld(string $baseUrl): string
{
    // remove trailing "/"
    $baseUrl = substr($baseUrl, 0, -1);
    $hostParts = explode('.', $baseUrl);

    return $hostParts[count($hostParts) - 1];
}

function getUrl(string $u): string
{
    $maxTry = 2;
    $errorMessage = [];
    for ($i = 0; $i < $maxTry; ++$i) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $u);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_PROTOCOLS, CURLPROTO_HTTPS);
        $responseData = curl_exec($ch);
        if(is_string($responseData)) {
            // success!
            curl_close($ch);

            return $responseData;
        }
        $errorNo = curl_errno($ch);
        if (!array_key_exists($errorNo, $errorMessage)) {
            $errorMessage[$errorNo] = curl_error($ch);
        }
        curl_close($ch);
        // sleep 1 second before trying again...
        sleep(1);
    }

    // didn't work after $maxTry attempts
    throw new RuntimeException('ERROR: ' . implode(', ', $errorMessage));
}

function getDisplayName(array $serverInstance): string
{
    if (array_key_exists('country_code', $serverInstance)) {
        return getEmojiFlag($serverInstance['country_code']);
    }
    if (!array_key_exists('display_name', $serverInstance)) {
        return $serverInstance['base_url'];
    }
    $displayName = $serverInstance['display_name'];
    if (!is_array($displayName)) {
        return $displayName;
    }
    // "en"
    if (array_key_exists('en', $displayName)) {
        return $displayName['en'];
    }
    // "en-US"
    if (array_key_exists('en-US', $displayName)) {
        return $displayName['en-US'];
    }
    // "en-*"
    foreach($displayName as $k => $v) {
        if(0 === strpos($k, 'en-')) {
            return $displayName[$k];
        }
    }

    // first mentioned language
    return array_values($displayName)[0];
}

function removeUriPrefix(string $uriStr): string
{
    if (0 === strpos($uriStr, 'tel:')) {
        return substr($uriStr, 4);
    }
    if (0 === strpos($uriStr, 'mailto:')) {
        return substr($uriStr, 7);
    }

    return $uriStr;
}

function getSupportType(string $uriStr): string
{
    if (0 === strpos($uriStr, 'tel:')) {
        return '☎️';
    }
    if (0 === strpos($uriStr, 'mailto:')) {
        return '📧';
    }

    return '🌐';
}

function mailErrorDiff(?string $statusUrl, string $mailTo, string $mailFrom, array $errorList): void
{
    $errorHistoryFile = __DIR__ . '/error_history.dat';
    $newError = [];
    $resolvedError = [];

    $errorHistory = [];
    if (file_exists($errorHistoryFile)) {
        // we have previous errors!
        $errorHistory = unserialize(file_get_contents($errorHistoryFile));
    }

    // check if we already knew about the errors in the current error list...
    foreach ($errorList as $baseUrl => $errorMsg) {
        if (!array_key_exists($baseUrl, $errorHistory)) {
            // we didn't know about it
            $newError[$baseUrl] = $errorMsg;
        }
    }

    // check for old errors that are now resolved...
    foreach ($errorHistory as $baseUrl => $errorMsg) {
        if (!array_key_exists($baseUrl, $errorList)) {
            // resolved
            $resolvedError[$baseUrl] = $errorMsg;
        }
    }

    file_put_contents($errorHistoryFile, serialize($errorList));

    if (0 === count($newError) && 0 === count($resolvedError)) {
        // nothing changed, do nothing
        return;
    }

    $mailMessage = '';
    if (0 !== count($newError)) {
        $mailMessage .= '*New Errors*' . "\n\n";
        foreach ($newError as $k => $v) {
            $mailMessage .= '    ' . $k . "\n    " . $v . "\n";
        }
        $mailMessage .= "\n";
    }
    if (0 !== count($resolvedError)) {
        $mailMessage .= '*Resolved Errors*' . "\n\n";
        foreach ($resolvedError as $k => $v) {
            $mailMessage .= '    ' . $k . "\n";
        }
        $mailMessage .= "\n";
    }

    if(0 !== count($errorList)) {
        $mailMessage .= '*All Errors*' . "\n\n";
        foreach ($errorList as $k => $v) {
            $mailMessage .= '    ' . $k . "\n    " . $v . "\n";
        }
        $mailMessage .= "\n";
    }

    if (null !== $statusUrl) {
        $mailMessage .= "-- \n";
        $mailMessage .= 'See ' . $statusUrl . ' for the current status.';
    }

    // mail the report
    mail(
        $mailTo,
        '[eduVPN] Server Status Notification',
        $mailMessage,
        "From: $mailFrom\nContent-Type: text/plain",
    );
}

function determineColor(string $baseVersion): string
{
    if ('?' === $baseVersion) {
        return 'plain';
    }

    if (in_array($baseVersion, VPN_LATEST_VERSION)) {
        // exact match
        return 'success';
    }

    $majorVersion = substr($baseVersion, 0, 1);
    if('2' === $majorVersion) {
        return 'error';
    }

    foreach (VPN_LATEST_VERSION as $latestVersion) {
        $latestMajorVersion = substr($latestVersion, 0, 1);
        if ($majorVersion !== $latestMajorVersion) {
            continue;
        }

        // are we development version?
        if (0 === strpos($baseVersion, '2.99.')) {
            return 'warning';
        }

        if (version_compare($baseVersion, $latestVersion, '>')) {
            return 'awesome';
        }

        return 'warning';
    }

    // not equal and not greater than any
    return 'warning';
}

function determineOsColor(string $osName): string
{
    $osMapping = [
        'Debian 9' => 'error',
        'Debian 10' => 'error',
        'Debian 11' => 'warning',
        'Debian 12' => 'success',
        'Debian 13' => 'awesome',
        'Ubuntu 22.04' => 'success',
        'Ubuntu 24.04' => 'success',
        'Enterprise Linux 7' => 'error',
        'Enterprise Linux 9' => 'success',
        'Fedora 35' => 'error',
        'Fedora 36' => 'error',
        'Fedora 37' => 'error',
        'Fedora 38' => 'error',
        'Fedora 39' => 'warning',
        'Fedora 40' => 'success',
        'Fedora 41' => 'success',
        'Fedora 42' => 'awesome',
    ];

    if(array_key_exists($osName, $osMapping)) {
        return $osMapping[$osName];
    }

    return 'neutral';
}

// now retrieve the .well-known/vpn-user-portal file from all servers
$serverInfoList = [];
$latestVersionCount = 0;
$serverCountList = [
    'secure_internet' => 0,
    'institute_access' => 0,
    'alien' => 0,
];
$versionStringCountList = [];
$osMapping = [
    'Debian 9' => ['+deb9+', '+deb+9', '+debian+9+', '+stretch+'],
    'Debian 10' => ['+deb10+', '+deb+10+', '+debian+10+', '+buster+'],
    'Debian 11' => ['+deb11+', '+deb+11+', '+debian+11+', '+bullseye+'],
    'Debian 12' => ['+debian+12+'],
    'Debian 13' => ['+debian+13+'],
    'Ubuntu 22.04' => ['+ubuntu+22.04+', '+jammy+'],
    'Ubuntu 24.04' => ['+ubuntu+24.04+'],
    'Enterprise Linux 7' => ['.el7'],
    'Enterprise Linux 9' => ['.el9'],
    'Fedora 35' => ['.fc35'],
    'Fedora 36' => ['.fc36'],
    'Fedora 37' => ['.fc37'],
    'Fedora 38' => ['.fc38'],
    'Fedora 39' => ['.fc39'],
    'Fedora 40' => ['.fc40'],
    'Fedora 41' => ['.fc41'],
    'Fedora 42' => ['.fc42'],
];
$osCountList = [];
$majorVersionCount = [];

foreach ($serverList as $srvInfo) {
    $baseUrl = $srvInfo['base_url'];
    $serverHost = parse_url($baseUrl, PHP_URL_HOST);
    $hasIpFour = checkdnsrr($serverHost, 'A');
    $hasIpSix = checkdnsrr($serverHost, 'AAAA');
    $srvInfo = array_merge(
        $srvInfo,
        [
            'v' => null,
            'h' => $serverHost,
            'hasIpFour' => $hasIpFour,
            'hasIpSix' => $hasIpSix,
            'errMsg' => null,
            'displayName' => getDisplayName($srvInfo),
        ],
    );

    ++$serverCountList[$srvInfo['server_type']];

    try {
        $infoJson = getUrl($baseUrl . '.well-known/vpn-user-portal');
        $infoData = json_decode($infoJson, true);
        if (!is_array($infoData)) {
            // probably the "well-known" file does not exist...
            $infoData = [];
        }
        $baseVersion = '?';
        $versionString = '?';
        if (array_key_exists('v', $infoData)) {
            $baseVersion = $infoData['v'];
            $versionString = $infoData['v'];
            if (false !== $dashPos = strpos($versionString, '-')) {
                $baseVersion = substr($versionString, 0, $dashPos);
            }
            $majorVersion = (int) substr($baseVersion, 0, 1);
            if(!array_key_exists($majorVersion, $majorVersionCount)) {
                $majorVersionCount[$majorVersion] = 0;
            }
            ++$majorVersionCount[$majorVersion];

            if (in_array($baseVersion, VPN_LATEST_VERSION)) {
                ++$latestVersionCount;
            }
            if(!array_key_exists($baseVersion, $versionStringCountList)) {
                $versionStringCountList[$baseVersion] = 0;
            }
            ++$versionStringCountList[$baseVersion];

            $osDetected = 'Unknown';
            foreach($osMapping as $k => $v) {
                foreach($v as $v2) {
                    if(false !== strpos($versionString, $v2)) {
                        $osDetected = $k;
                    }
                }
            }
            if(!array_key_exists($osDetected, $osCountList)) {
                $osCountList[$osDetected] = 0;
            }
            ++$osCountList[$osDetected];
        }
        $srvInfo['v'] = $baseVersion;
        $srvInfo['vDisplay'] = $versionString;

        if(!array_key_exists($baseUrl, $serverHistory)) {
            $serverHistory[$baseUrl] = [];
        }
        $serverHistory[$baseUrl][$dateTime->format(DateTimeImmutable::ATOM)] = $versionString;
    } catch (RuntimeException $e) {
        $srvInfo['errMsg'] = $e->getMessage();
        $errorList[$baseUrl] = $e->getMessage();
        if(!array_key_exists($baseUrl, $serverHistory)) {
            $serverHistory[$baseUrl] = [];
        }

        $serverHistory[$baseUrl][$dateTime->format(DateTimeImmutable::ATOM)] = $e->getMessage();
    }

    $serverInfoList[$baseUrl] = $srvInfo;
}

file_put_contents($serverHistoryFile, serialize($serverHistory));

if (null !== $configData['mailTo']) {
    // remove notoriously unreliable servers from the error list
    foreach ($configData['doNotMonitorList'] as $baseUrl) {
        if (array_key_exists($baseUrl, $errorList)) {
            unset($errorList[$baseUrl]);
        }
    }
    $statusUrl = $configData['statusUrl'] ?? null;
    mailErrorDiff($statusUrl, $configData['mailTo'], $configData['mailFrom'], $errorList);
}

arsort($versionStringCountList);
arsort($osCountList);

?>
<!DOCTYPE html>

<html lang="en-US" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Server Status</title>
<?php if ($configData['embedCss']): ?>
    <style><?=file_get_contents(__DIR__ . '/simple.css'); ?></style>
    <style><?=file_get_contents(__DIR__ . '/screen.css'); ?></style>
<?php else: ?>
    <link href="simple.css" rel="stylesheet">
    <link href="screen.css" rel="stylesheet">
<?php endif; ?>
</head>
<body>
<header>
<h1>eduVPN</h1>
</header>
<main>
<h2>Server Status</h2>
<table>
<thead>
<tr><th>Type</th><th>#Servers</th></tr>
</thead>
<tbody>
<?php foreach ($serverCountList as $serverType => $serverCount): ?>
<?php if (0 !== $serverCount): ?>
    <tr>
        <td>
<?php if ('secure_internet' === $serverType): ?>
            <span title="Secure Internet">🌍 Secure Internet</span>
<?php elseif ('institute_access' === $serverType): ?>
            <span title="Institute Access">🏛️ Institute Access</span>
<?php else: ?>
            <span title="Alien">👽 Alien</span>
<?php endif; ?>
        </td>
        <td><?=$serverCount; ?></td>
    </tr>
<?php endif; ?>
<?php endforeach; ?>
</tbody>
</table>
<?php if (1 === count(VPN_LATEST_VERSION)): ?>
<p>The current <span class="success">STABLE</span> release is <?=implode(', ', VPN_LATEST_VERSION); ?>. Currently <?=floor(100 * $latestVersionCount / count($serverList)); ?>% of all servers run this version.</p>
<?php else: ?>
<p>The current <span class="success">STABLE</span> releases are [<?=implode(', ', VPN_LATEST_VERSION); ?>]. Currently <?=floor(100 * $latestVersionCount / count($serverList)); ?>% of all servers run one of these versions.</p>
<?php endif; ?>
<table>
	<thead>
		<th>Type</th><th>Server</th><th>Version</th><th>User Support</th>
<?php if($showTechContact): ?>
		<th>Technical Contact</th>
<?php endif; ?>
	</tr>
	</thead>
<tbody>
<?php foreach ($serverInfoList as $baseUrl => $serverInfo): ?>
    <tr id="<?=$baseUrl; ?>">
        <td>
<?php if ('secure_internet' === $serverInfo['server_type']): ?>
            <span title="Secure Internet">🌍</span>
<?php elseif ('institute_access' === $serverInfo['server_type']): ?>
            <span title="Institute Access">🏛️</span>
<?php else: ?>
            <span title="Alien">👽</span>
<?php endif; ?>
        </td>
        <td>
            <a href="#<?=$baseUrl; ?>"><?=$serverInfo['displayName']; ?></a> <small>[<a href="<?=$baseUrl; ?>"><?=$serverInfo['h']; ?></a>]</small>
<?php if ($serverInfo['hasIpFour']): ?>
                <sup><span class="success" title="IPv4">4</span></sup>
<?php else: ?>
                <sup><span class="warning" title="No IPv4">4</span></sup>
<?php endif; ?>
<?php if ($serverInfo['hasIpSix']): ?>
                <sup><span class="success" title="IPv6">6</span></sup>
<?php else: ?>
                <sup><span class="warning" title="No IPv6">6</span></sup>
<?php endif; ?>
        </td>
        <td>
<?php if (null === $serverInfo['v']): ?>
<?php if (null !== $serverInfo['errMsg']): ?>
            <span class="error" title="<?=htmlentities($serverInfo['errMsg']); ?>">Error</span>
<?php else: ?>
            <span class="error">Error</span>
<?php endif; ?>
<?php else: ?>
            <span class="<?=determineColor($serverInfo['v']); ?>"><?=$serverInfo['vDisplay']; ?></span>
<?php endif; ?>
<?php if ($showHistory && array_key_exists($baseUrl, $serverHistory)): ?>
<details>
	<summary>History</summary>
	<ul>
<?php foreach($serverHistory[$baseUrl] as $dT => $historyMsg): ?>
	<li><?=$dT; ?>: <?=$historyMsg; ?></li>
<?php endforeach; ?>
</ul>
</details>
<?php endif; ?>
		</td>
		<td>
<?php if (array_key_exists('support_contact', $serverInfo) && 0 !== count($serverInfo['support_contact'])): ?>
<?php foreach ($serverInfo['support_contact'] as $supportContact): ?>
            <a class="nodeco" href="<?=$supportContact; ?>"><?=getSupportType($supportContact); ?></a>
<?php endforeach; ?>
<?php else: ?>
	<em>N/A</em>
<?php endif; ?>
        </td>
<?php if($showTechContact): ?>
<?php if(null === $serverInfo['tech_contact']): ?>
		<td><em>N/A</em></td>
<?php else: ?>
		<td>
			<a class="nodeco" href="mailto:<?=implode(',', $serverInfo['tech_contact']); ?>">📧</a>
		</td>
<?php endif; ?>
<?php endif; ?>
    </tr>
<?php endforeach; ?>
</tbody>
</table>
<h3 id="versions">Versions</h3>
<p>Breakdown by VPN server version used by the servers.</p>
<table>
	<thead>
		<tr><th>Major Version</th><th>#Servers</th></tr>
	</thead>
	<tbody>
<?php foreach($majorVersionCount as $k => $v): ?>
        <tr><td><?=$k; ?>.x</td><td><?=$v; ?> (<?=(int) ($v / array_sum($majorVersionCount) * 100); ?>%)</td></tr>
<?php endforeach; ?>
    </tbody>
</table>

<table>
	<thead>
		<tr><th>Version</th><th>#Servers</th></tr>
	</thead>
	<tbody>
<?php foreach($versionStringCountList as $k => $v): ?>
		<tr><td><span class="<?=determineColor($k); ?>"><?=$k; ?></span></td><td><?=$v; ?></td><tr>
<?php endforeach; ?>
	</tbody>
</table>
<h3 id="operating-systems">Operating Systems</h3>
<p>Breakdown by operating system used by the servers.</p>
<table>
	<thead>
		<tr><th>OS</th><th>#Servers</th></tr>
	</thead>
	<tbody>
<?php foreach($osCountList as $k => $v): ?>
		<tr><td><span class="<?=determineOsColor($k); ?>"><?=$k; ?></span></td><td><?=$v; ?></td></tr>
<?php endforeach; ?>
	</tbody>
</table>
</main>
<footer>
Generated on <?=$dateTime->format(DateTime::ATOM); ?>
</footer>
</body>
</html>
