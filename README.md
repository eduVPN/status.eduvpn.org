# status.eduvpn.org

## Clone 

```bash
$ git clone https://codeberg.org/eduVPN/status.eduvpn.org
```

## Configure

```bash
$ cp config.php.example config.php
```

Modify and update `config.php` as desired.

## Run

```bash
$ php version_info.php > output.html
```

This will generate a report in `output.html` you can view with your browser, or
upload to a web server.

## Cron


```
@daily	/usr/bin/php /path/to/version_info.php > /var/www/html/version_info.html.tmp && mv /var/www/html/version_info.html.tmp /var/www/html/version_info.html
```
